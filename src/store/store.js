import { writable } from 'svelte/store';

export const jugadores = writable([]);
export const rondas = writable(0);
export const tiempo= writable(0);
export const espera = writable(0);
export const turno = writable(0);

export let cantidadJugadores;
export let nombreSiguiente = "";
export let termino = false;
let turnoActual = 0;
let ronda = 2;

jugadores.subscribe(value => {cantidadJugadores = value;});
export function reset(){
	jugadores.update(l => l = []);
}
export function agregarJugador(nombre){
	jugadores.update(l => l = [...l, {nombre:nombre,puntos: 0}]);
}
export function eliminarJugador(nombre){
	jugadores.update(l => l = l.filter((j)=> j.nombre != nombre));
}
export function agregarPunto(jg){
	siguienteTurno();
	let old = cantidadJugadores;
	old.forEach((j,i)=>{
		if (jg == j){
			old[i].puntos = old[i].puntos + 1;
		}
	})
	jugadores.update(j => j = old);

}

export function editarRondas(cantidad){
	ronda = cantidad;
	rondas.update(r => r = cantidad);
}
export function editarTiempo(cantidad){
	tiempo.update(r => r = cantidad);
}
export function editarEspera(cantidad){
	espera.update(r => r = cantidad);
	nombreSiguiente = cantidadJugadores[0].nombre;
}

export function siguienteTurno(){
	turnoActual++;
	console.log(turnoActual + " - "+ ronda*cantidadJugadores.length);
	if (turnoActual == ronda*cantidadJugadores.length){
		termino = true;
	}
	let index = turnoActual % cantidadJugadores.length;
	nombreSiguiente = cantidadJugadores[index].nombre;
}